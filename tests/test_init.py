from pkg_resources import parse_version as V


def test_version():
    from qlknn import __version__

    # Check if version is parsable
    V(__version__)
